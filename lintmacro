#!/bin/env bash
LINT_TYPE=
LINT_VERBOSE=0
LINT_FILES=[]
USAGE_MSG="Usage: $(basename "$0"): [-t \"string\"] [-v] files
parameter:
    -t {ansible|bash}  -  query parameter to use
    -v  -  print API-Call used
"

usage() {
    echo "$USAGE_MSG" >&2
    exit 2
}

lint_ansible() {
    echo "------- BEGIN - ansible-lint -------"
    if [[ LINT_VERBOSE -eq 1 ]]; then
        echo "ansible-lint --nocolor -q $1"
    fi
    ansible-lint --nocolor -q "$1"
    echo "------- END -------"
    lint_yaml "$1"
}

lint_bash() {
    if [[ LINT_VERBOSE -eq 1 ]]; then
        echo "shellcheck $1"
    fi
    shellcheck "$1"
}

lint_yaml() {
    echo "------- BEGIN - yamllint -------"
    if [[ LINT_VERBOSE -eq 1 ]]; then
        echo "yamllint $1"
    fi
    yamllint "$1"
    echo "------- END -------"
}

# Process flags

while getopts 't:v' OPTION; do
    case $OPTION in
        t)
            LINT_TYPE="$OPTARG"
            ;;
        v)
            LINT_VERBOSE=1
            ;;
        ?)
            usage
            ;;
    esac
done
shift $((OPTIND - 1))

# Process parameter

if [[ $# -gt 0 ]]; then
    LINT_FILES=( "$@" )
fi

case $LINT_TYPE in
    ansible)
        for file in "${LINT_FILES[@]}"; do
            lint_ansible "$file"
        done
        ;;
    bash)
        for file in "${LINT_FILES[@]}"; do
            lint_bash "$file"
        done
        ;;
    *)
        usage
        ;;
esac

